package com.zuitt.example;
import java.util.Scanner;

public class s1A1 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = userInput.nextLine();

        System.out.println("Last Name: ");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade: ");
        float subject1 = userInput.nextFloat();

        System.out.println("Second Subject Grade: ");
        float subject2 = userInput.nextFloat();

        System.out.println("Third Subject Grade: ");
        float subject3 = userInput.nextFloat();

        System.out.println("Good day, " + firstName + " " + lastName);

        float averageGrade = (subject1 + subject2 + subject3) / 3;
        System.out.println("Your grade average is: " + averageGrade);

    }
}
