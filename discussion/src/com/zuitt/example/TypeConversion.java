package com.zuitt.example;
import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");

        // 1st method
       // double age = Double.parseDouble(userInput.nextLine());

        // 2nd method
        double age = userInput.nextDouble();
        System.out.println("This is a confirmation that you are " + age +" years old");

        System.out.println("Enter a first number: ");
        int num1 = userInput.nextInt();

        System.out.println("Enter a second number: ");
        int num2 = userInput.nextInt();

        int sum = num1 + num2;
        System.out.println("The sum of both number are " + sum);

    }
}
