package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Username: ");

        String userName = myObj.nextLine(); // store the input until the next line changes
         System.out.println("Username is: " + userName);

        // System.out.println(userName + userName);

    }
}
