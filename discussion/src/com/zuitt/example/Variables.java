// a package in Java is used to group related classes. Think of it as a folder and a file directory.

// Package creation follows "reverse name notation" for the naming convention.

package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        // Variable
        // Syntax: dataType identifier
        int age;
        char middleInitial;

        // variable declaration vs initialization
        int x;
        int y = 0;

        // initialization after declaration
        x = 1;
        System.out.println("The value of y is " + y + " the value of x is " + x);

        // PRIMITIVE DATA TYPES
            // predefined within the Java Programming Language which is used for single-valued  variables with limited capabilities.

        // int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long
        // L is added at the end of the number that we recognized as long
        long worldPopulation = 8000000000L;
        System.out.println(worldPopulation);

        // float
        // number for decimal places
        float piFloat = 3.141592265359f;
        System.out.println(piFloat);

        //double
        double doubleFloat = 3.141592265359;
        System.out.println(doubleFloat);

        // char - single character ONLY
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constants
        // Java uses the "final" keyword to the variables value that cannot be changed.
        // in this case "PRINCIPAL" is all on caps to specify that it will not be change
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //NON-PRIMITIVE DATA TYPES
            // also known as reference data types refer to instances or objects
            // do not directly store the value of that variable

        // String
            // Stores a sequence or array of characters.
            // String are actually objects that can use methods

        String userName = "JSmith";
        System.out.println(userName);

        // Sample String Method
        int stringLength = userName.length();
        System.out.println(stringLength);

    }
}
