// Main class
  // entry point for our java program
    // main class has 1 method inside, the "main method" => run code
    // access modifier like public or private

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}